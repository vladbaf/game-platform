package kata.education.platform.gameplatform.dao.impl.model.chat;

import kata.education.platform.gameplatform.dao.abstracts.model.chat.MessageDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.model.entity.chat.Message;
import org.springframework.stereotype.Repository;

@Repository
public class MessageDaoImpl extends ReadWriteDaoImpl <Message, Long> implements MessageDao {
}
