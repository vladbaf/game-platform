package kata.education.platform.gameplatform.dao.impl.model.post;

import kata.education.platform.gameplatform.dao.abstracts.model.post.PostDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.model.entity.post.Post;
import org.springframework.stereotype.Repository;

@Repository
public class PostDaoImpl extends ReadWriteDaoImpl<Post, Long> implements PostDao {
}