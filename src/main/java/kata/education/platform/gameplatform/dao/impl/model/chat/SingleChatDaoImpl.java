package kata.education.platform.gameplatform.dao.impl.model.chat;

import kata.education.platform.gameplatform.dao.abstracts.model.chat.SingleChatDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.model.entity.chat.SingleChat;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class SingleChatDaoImpl extends ReadWriteDaoImpl<SingleChat, Long> implements SingleChatDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public boolean isExistsChatByUsersId(Long userOneId, Long userTwoId) {
        long count = (long) entityManager.createQuery("SELECT COUNT(e) FROM SingleChat e WHERE e.userOne.id = :userOneId AND e.userTwo.id = :userTwoId")
                .setParameter("userOneId", userOneId)
                .setParameter("userTwoId", userTwoId)
                .getSingleResult();
        return count > 0;
    }
}
