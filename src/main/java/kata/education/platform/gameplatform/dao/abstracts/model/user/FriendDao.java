package kata.education.platform.gameplatform.dao.abstracts.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.user.Friend;

import java.util.Optional;

public interface FriendDao extends ReadWriteDao<Friend, Long> {

    Optional<Friend> findFriendByIds(Long userId, Long friendId);
}
