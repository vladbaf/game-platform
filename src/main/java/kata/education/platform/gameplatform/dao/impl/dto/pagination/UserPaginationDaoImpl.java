package kata.education.platform.gameplatform.dao.impl.dto.pagination;

import kata.education.platform.gameplatform.dao.abstracts.dto.pagination.PaginationDao;
import kata.education.platform.gameplatform.model.dto.user.UserDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Repository
public class UserPaginationDaoImpl<T> implements PaginationDao<UserDto> {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserDto> getItems(Map<String, Object> parameters) {
        int currentPage = (int) parameters.get("currentPage");
        int itemsOnPage = (int) parameters.get("itemsOnPage");

        Query query = entityManager.createQuery("SELECT new kata.education.platform.gameplatform.model.dto.user.UserDto(" +
                                                        "id," +
                                                        "nickname," +
                                                        "coins," +
                                                        "avatar" +
                                                        ") FROM User", UserDto.class);
        query.setFirstResult((currentPage - 1) * itemsOnPage);
        query.setMaxResults(itemsOnPage);
        return query.getResultList();
    }

    @Override
    public Long getItemsCount(Map<String, Object> parameters) {
        Query query = entityManager.createQuery("SELECT COUNT (u) FROM User u");
        return (Long) query.getSingleResult();
    }
}
