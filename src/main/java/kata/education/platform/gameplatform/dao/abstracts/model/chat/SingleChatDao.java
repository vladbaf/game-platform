package kata.education.platform.gameplatform.dao.abstracts.model.chat;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.chat.SingleChat;

public interface SingleChatDao extends ReadWriteDao<SingleChat, Long> {

    boolean isExistsChatByUsersId(Long userOneId, Long userTwoId);
}
