package kata.education.platform.gameplatform.dao.abstracts.model.post;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.post.Post;

public interface PostDao extends ReadWriteDao<Post, Long> {
}