package kata.education.platform.gameplatform.dao.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.user.RoleDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.dao.util.SingleResultUtil;
import kata.education.platform.gameplatform.model.entity.user.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
public class RoleDaoImpl extends ReadWriteDaoImpl<Role, Long> implements RoleDao {
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public Optional<Role> getByName(String name) {
        String hql = "FROM Role WHERE name = :name";
        TypedQuery<Role> query = entityManager.createQuery(hql, Role.class).setParameter("name", name);
        return SingleResultUtil.getSingleResultOrNull(query);

    }
}
