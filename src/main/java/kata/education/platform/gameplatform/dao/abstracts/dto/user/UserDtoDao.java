package kata.education.platform.gameplatform.dao.abstracts.dto.user;

import kata.education.platform.gameplatform.model.dto.user.UserDto;

import java.util.Optional;

public interface UserDtoDao {
    Optional<UserDto> getUserById(Long id);
}
