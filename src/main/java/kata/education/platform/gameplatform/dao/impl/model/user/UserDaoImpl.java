package kata.education.platform.gameplatform.dao.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.user.UserDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.dao.util.SingleResultUtil;
import kata.education.platform.gameplatform.model.entity.user.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
public class UserDaoImpl extends ReadWriteDaoImpl<User, Long> implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<User> getByEmail(String email) {
        String hql = "FROM User WHERE email = :email";
        TypedQuery<User> query = entityManager.createQuery(hql, User.class).setParameter("email", email);
        return SingleResultUtil.getSingleResultOrNull(query);
    }


    @Override
    public boolean existsByEmail(String email) {
        long count = (long) entityManager.createQuery("SELECT COUNT(e) FROM User e WHERE e.email = :email")
                .setParameter("email", email)
                .getSingleResult();
        return count > 0;
    }
}
