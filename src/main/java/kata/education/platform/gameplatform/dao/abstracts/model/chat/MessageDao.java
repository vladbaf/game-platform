package kata.education.platform.gameplatform.dao.abstracts.model.chat;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.chat.Message;


public interface MessageDao extends ReadWriteDao<Message, Long> {
}
