package kata.education.platform.gameplatform.dao.impl.dto.user;

import kata.education.platform.gameplatform.dao.abstracts.dto.user.UserDtoDao;
import kata.education.platform.gameplatform.dao.util.SingleResultUtil;
import kata.education.platform.gameplatform.model.dto.user.UserDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Repository
public class UserDtoDaoImpl implements UserDtoDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<UserDto> getUserById(Long id) {
        return SingleResultUtil.getSingleResultOrNull(entityManager.createQuery("SELECT new kata.education.platform.gameplatform.model.dto.user.UserDto (u.id, u.nickname, u.coins, u.avatar)" +
                        " FROM User u WHERE u.id = :id", UserDto.class)
                .setParameter("id", id));
    }
}
