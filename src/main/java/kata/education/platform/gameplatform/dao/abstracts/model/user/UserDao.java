package kata.education.platform.gameplatform.dao.abstracts.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.user.User;

import java.util.Optional;

public interface UserDao extends ReadWriteDao<User, Long> {

    Optional<User> getByEmail(String email);

    boolean existsByEmail(String email);

    Optional<User> getById(Long id);
}
