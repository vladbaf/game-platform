package kata.education.platform.gameplatform.dao.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.user.FollowerDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.dao.util.SingleResultUtil;
import kata.education.platform.gameplatform.model.entity.user.Follower;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.Optional;


@Repository
public class FollowerDaoImpl extends ReadWriteDaoImpl<Follower, Long> implements FollowerDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Follower> findFollowerById(Long followedUserId, Long followerId) {
        String hql = "FROM Follower e WHERE e.followedUser.id = :followedUserId and e.follower.id = :followerId";
        TypedQuery<Follower> query = entityManager.createQuery(hql, Follower.class)
                .setParameter("followedUserId", followedUserId)
                .setParameter("followerId", followerId);
        return SingleResultUtil.getSingleResultOrNull(query);
    }

}