package kata.education.platform.gameplatform.dao.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.user.FriendDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.dao.util.SingleResultUtil;
import kata.education.platform.gameplatform.model.entity.user.Follower;
import kata.education.platform.gameplatform.model.entity.user.Friend;
import kata.education.platform.gameplatform.model.entity.user.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class FriendDaoImpl extends ReadWriteDaoImpl<Friend, Long> implements FriendDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Friend> findFriendByIds(Long userId, Long friendId) {
        String hql = "FROM Friend e WHERE e.user.id = :userId and e.friend.id = :friendId";
        TypedQuery<Friend> query = entityManager.createQuery(hql, Friend.class).setParameter("userId", userId).setParameter("friendId", friendId);
        return SingleResultUtil.getSingleResultOrNull(query);
    }

}
