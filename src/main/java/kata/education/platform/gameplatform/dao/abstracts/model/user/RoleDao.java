package kata.education.platform.gameplatform.dao.abstracts.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.user.Role;

import java.util.Optional;

public interface RoleDao extends ReadWriteDao<Role, Long> {
    Optional<Role> getByName(String name);
}
