package kata.education.platform.gameplatform.dao.abstracts.model.file;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.file.FileInfo;

public interface FileInfoDao extends ReadWriteDao<FileInfo, Long> {

}
