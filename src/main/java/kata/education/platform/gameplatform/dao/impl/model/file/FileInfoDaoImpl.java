package kata.education.platform.gameplatform.dao.impl.model.file;

import kata.education.platform.gameplatform.dao.abstracts.model.file.FileInfoDao;
import kata.education.platform.gameplatform.dao.impl.model.ReadWriteDaoImpl;
import kata.education.platform.gameplatform.model.entity.file.FileInfo;
import org.springframework.stereotype.Repository;

@Repository
public class FileInfoDaoImpl extends ReadWriteDaoImpl<FileInfo, Long> implements FileInfoDao {
}
