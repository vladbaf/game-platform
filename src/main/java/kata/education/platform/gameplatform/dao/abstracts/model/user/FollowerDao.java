package kata.education.platform.gameplatform.dao.abstracts.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.model.entity.user.Follower;

import java.util.Optional;

public interface FollowerDao extends ReadWriteDao<Follower , Long> {
    Optional<Follower> findFollowerById(Long followedUserId, Long followerId);
}