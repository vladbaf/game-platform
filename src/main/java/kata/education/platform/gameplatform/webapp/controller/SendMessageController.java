package kata.education.platform.gameplatform.webapp.controller;

import kata.education.platform.gameplatform.model.dto.message.MessageCreateDto;
import kata.education.platform.gameplatform.model.dto.message.MessageDto;
import kata.education.platform.gameplatform.model.entity.chat.Message;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.chat.MessageService;
import kata.education.platform.gameplatform.webapp.converter.MessageMapper;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;


@Controller
@AllArgsConstructor
public class SendMessageController {

    private final MessageMapper messageMapper;
    private final MessageService messageService;

    @MessageMapping("/chat")
    @SendTo("topic/messages")
    public MessageDto sendMessage(@Payload MessageCreateDto messageCreateDto) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Message newMessage = messageMapper.toEntity(messageCreateDto, user.getId());

        newMessage.setPersistDate(LocalDateTime.now());

        messageService.persist(newMessage);
        
        return messageMapper.toDto(newMessage);

    }
}
