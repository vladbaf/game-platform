package kata.education.platform.gameplatform.webapp.controller;

import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.file.FileInfoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/api/upload")
public class UploadController {

    private final FileInfoService fileService;

    public UploadController(FileInfoService fileService) {
        this.fileService = fileService;
    }


    @RequestMapping(value = "/single", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> uploadSingleFile(@RequestParam("file") MultipartFile file) {
        User userOwner = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body("The uploaded file was not found");
        }
        if (file.getSize() > 1048576) {
            throw new MultipartException("The file size exceeds 1 MB");
        }
        System.out.println(file.getSize());
        return ResponseEntity.ok().body(fileService.uploadFile(file, userOwner));
    }
}
