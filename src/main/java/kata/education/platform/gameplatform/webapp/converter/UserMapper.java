package kata.education.platform.gameplatform.webapp.converter;

import kata.education.platform.gameplatform.model.dto.request.OAuth2RegisterDto;
import kata.education.platform.gameplatform.model.dto.request.RegisterDto;
import kata.education.platform.gameplatform.model.dto.user.UserUpdateDto;
import kata.education.platform.gameplatform.model.entity.user.Role;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.user.RoleService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleNotFoundException;
import java.util.Optional;

@Service
@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Autowired
    private RoleService roleService;
    @Autowired
    protected PasswordEncoder passwordEncoder;

    @Mapping(target = "role", constant = "ROLE_USER", qualifiedByName = "roleSetter")
    @Mapping(target = "password", expression = "java(passwordEncoder.encode(registerDTO.getPassword()))")
    public abstract User toEntity(RegisterDto registerDTO);

    @Mapping(target = "role", constant = "ROLE_USER", qualifiedByName = "roleSetter")
    public abstract User toEntity(OAuth2RegisterDto registerDTO);

    @Named("roleSetter")
    public Role enrichUser(String roleName) throws RoleNotFoundException {
        Optional<Role> role = roleService.getByName(roleName);
        if (role.isEmpty()) {
            throw new RoleNotFoundException(String.format("Role: %s not found", roleName));
        }
        return role.get();
    }

    @Mapping(source = "id", target = "id")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "nickname", target = "nickname")
    @Mapping(source = "roleName", target = "role", qualifiedByName= "roleSetter")
    public abstract User toEntity(UserUpdateDto userUpdateDto, @MappingTarget User user);
}
