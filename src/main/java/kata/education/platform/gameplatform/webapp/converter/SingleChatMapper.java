package kata.education.platform.gameplatform.webapp.converter;

import kata.education.platform.gameplatform.model.dto.chat.ChatDto;
import kata.education.platform.gameplatform.model.entity.chat.SingleChat;
import kata.education.platform.gameplatform.model.entity.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;


@Service
@Mapper(componentModel = "spring")
public abstract class SingleChatMapper {


    @Mapping(target = "id", ignore = true)
    @Mapping(target = "chat.type", constant = "SINGLE_CHAT")

    public abstract SingleChat toEntity(User userOne, User userTwo, ChatDto chat);

    @Mapping(target = "image", source = "chat.image")
    @Mapping(target = "type", constant = "SINGLE_CHAT")
    @Mapping(target = "id", source = "chat.id")
    @Mapping(target = "title", source = "chat.title")
    @Mapping(target = "lastMessage", ignore = true)

    public abstract ChatDto toDto(SingleChat singleChat);


}
