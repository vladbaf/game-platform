package kata.education.platform.gameplatform.webapp.converter;

import kata.education.platform.gameplatform.exception.UserNotFoundException;
import kata.education.platform.gameplatform.model.dto.message.MessageCreateDto;
import kata.education.platform.gameplatform.model.dto.message.MessageDto;
import kata.education.platform.gameplatform.model.entity.chat.Message;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;


@Service
@Mapper(componentModel = "spring")
public abstract class MessageMapper {

    @Autowired
    private UserService userService;

    @Mapping(source = "userId", target = "userSender", qualifiedByName = "userSetter")
    public abstract Message toEntity(MessageCreateDto messageCreateDto, Long userId);

    public abstract MessageDto toDto(Message message);

    @Named("userSetter")
    public User userSetter(Long userId) throws UserNotFoundException{

        return userService.getById(userId).orElseThrow(
                ()-> new EntityNotFoundException(String.format("User  with id %s does not exist in the database", userId)));
    }
}
