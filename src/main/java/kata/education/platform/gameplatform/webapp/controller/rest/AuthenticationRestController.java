package kata.education.platform.gameplatform.webapp.controller.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kata.education.platform.gameplatform.exception.UserNotFoundException;
import kata.education.platform.gameplatform.model.dto.request.AuthenticationRequestDto;
import kata.education.platform.gameplatform.model.dto.request.OAuth2RequestDto;
import kata.education.platform.gameplatform.model.dto.request.RegisterDto;
import kata.education.platform.gameplatform.model.dto.response.JwtResponse;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.security.AuthenticationService;
import kata.education.platform.gameplatform.security.oauth.service.OAuth2UserService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.webapp.converter.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Контроллер для аутентификации и регистрации")
@RestController
@RequestMapping(value = "/api/auth")
@AllArgsConstructor
public class AuthenticationRestController {

    private final AuthenticationService authenticationService;
    private final UserService userService;
    private final UserMapper userMapper;
    private final OAuth2UserService oAuth2UserService;

    @ApiOperation(value = "Получение токена")
    @PostMapping(value = "/token")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully token create"),
            @ApiResponse(code = 400, message = "Invalid login or password")
    })
    public ResponseEntity<JwtResponse> authUser(@RequestBody AuthenticationRequestDto authenticationRequestDto) {
        JwtResponse jwtResponse = authenticationService.authenticateAndGetToken(authenticationRequestDto);
        return ResponseEntity.ok(jwtResponse);
    }

    @ApiOperation(value = "Регистрация нового пользователя")
    @PostMapping("register")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Registration user success"),
            @ApiResponse(code = 400, message = "User with email: user@mail.com exist in the database")
    })
    public ResponseEntity<?> registrationNewUser(@RequestBody RegisterDto registerDto) {

        if (userService.existsByEmail(registerDto.getEmail())) {
            throw new UserNotFoundException(String.format("User with email: %s exist in the database", registerDto.getEmail()));
        }
        userService.persist(userMapper.toEntity(registerDto));
        return new ResponseEntity<>("Registration user success", HttpStatus.CREATED);
    }


    @PostMapping("/oauth2")
    public ResponseEntity<JwtResponse> authOAuth2User(@RequestBody OAuth2RequestDto oAuth2RequestDto) {

        User userToAuthenticate = oAuth2UserService.loadUserFromAccessToken(oAuth2RequestDto);
        JwtResponse jwtResponse = authenticationService.authenticateOAuth2UserAndGetToken(userToAuthenticate);

        return ResponseEntity.ok(jwtResponse);
    }


}
