package kata.education.platform.gameplatform.webapp.controller.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kata.education.platform.gameplatform.model.dto.chat.ChatDto;
import kata.education.platform.gameplatform.model.entity.chat.Message;
import kata.education.platform.gameplatform.model.entity.chat.SingleChat;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.chat.MessageService;
import kata.education.platform.gameplatform.service.abstracts.model.chat.SingleChatService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.webapp.converter.SingleChatMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

@Api(value = "Контроллер чата")
@RestController
@RequestMapping("api/chats/")
public class ChatRestController {

    private final UserService userService;
    private final SingleChatMapper singleChatMapper;
    private final SingleChatService singleChatService;
    private final MessageService messageService;

    @Autowired
    public ChatRestController(UserService userService, SingleChatMapper singleChatMapper, SingleChatService singleChatService, MessageService messageService) {
        this.userService = userService;
        this.singleChatMapper = singleChatMapper;
        this.singleChatService = singleChatService;
        this.messageService = messageService;
    }


    @ApiOperation(value = "Создание одиночного чата")
    @GetMapping("single-chats/user/{userId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Single chat created"),
            @ApiResponse(code = 400, message = "An error occurred while creating the chat")
    })
    public ResponseEntity<?> createSingleChat(@RequestBody ChatDto chatDto, @PathVariable("userId") Long userId) {
        User userOne = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> userTwo = userService.getById(userId);
        if (userTwo.isEmpty()) {
            return ResponseEntity.badRequest().body("User with id " + userId + " not found!");
        }
        if (Objects.equals(userOne.getId(), userTwo.get().getId())) {
            return ResponseEntity.badRequest().body("You can't create a chat with yourself");
        }
        if (singleChatService.isExistsChatByUsersId(userOne.getId(), userId)) {
            return ResponseEntity.badRequest().body("You can't create an existing chat");
        }
        SingleChat chat = singleChatMapper.toEntity(userOne, userTwo.get(), chatDto);
        Message message = new Message();
        message.setMessage(chatDto.getLastMessage());
        message.setChat(chat.getChat());
        message.setUserSender(userOne);
        singleChatService.persist(chat);
        messageService.persist(message);

        ChatDto chatDto1 = singleChatMapper.toDto(chat);
        chatDto1.setLastMessage(chatDto.getLastMessage());

        return ResponseEntity.ok(chatDto1);
    }

}
