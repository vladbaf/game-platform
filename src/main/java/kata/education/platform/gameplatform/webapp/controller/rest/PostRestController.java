package kata.education.platform.gameplatform.webapp.controller.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kata.education.platform.gameplatform.exception.PostCreationException;
import kata.education.platform.gameplatform.model.dto.post.PostCreateDto;
import kata.education.platform.gameplatform.model.dto.post.PostDto;
import kata.education.platform.gameplatform.model.entity.post.Post;
import kata.education.platform.gameplatform.service.abstracts.model.post.PostService;
import kata.education.platform.gameplatform.webapp.converter.PostMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Api(value = "Контроллер для работы с сущностью Post")
@RestController
@RequestMapping("/api/post")
@AllArgsConstructor
@Validated
public class PostRestController {

    private PostService postService;
    private PostMapper postMapper;

    @ApiOperation(value = "Создание поста")
    @PostMapping("/create")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post creation successful"),
            @ApiResponse(code = 400, message = "Can't create post"),
    })
    @ExceptionHandler(PostCreationException.class)
    public ResponseEntity<?> createPost(@RequestBody @Valid PostCreateDto postCreateDto) {
        Post newPost = postMapper.toEntity(postCreateDto);
        postService.persist(newPost);
        PostDto postDto = postMapper.toDto(newPost);
        return ResponseEntity.ok().body(postDto);
    }

}