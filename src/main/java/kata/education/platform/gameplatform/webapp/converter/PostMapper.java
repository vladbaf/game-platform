package kata.education.platform.gameplatform.webapp.converter;

import kata.education.platform.gameplatform.exception.UserNotFoundException;
import kata.education.platform.gameplatform.model.dto.post.PostCreateDto;
import kata.education.platform.gameplatform.model.dto.post.PostDto;
import kata.education.platform.gameplatform.model.entity.post.Post;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Mapper(componentModel = "spring")
public abstract class PostMapper {
    @Autowired
    UserService userService;

    @Mapping(target = "user", source = "userId",  qualifiedByName = "userSetter")
    public abstract Post toEntity(PostCreateDto postCreateDto);

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "nickname", source = "user.nickname")
    public abstract PostDto toDto(Post post);

    @Named("userSetter")
    public User enrichPost(Long id) throws UserNotFoundException {
        Optional<User> user = userService.getById(id);
        if (user.isEmpty()) {
            throw new UserNotFoundException(String.format("User: %s not found", id));
        }
        return user.get();
    }
}