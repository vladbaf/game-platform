package kata.education.platform.gameplatform.webapp.controller.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kata.education.platform.gameplatform.model.dto.user.UserDto;
import kata.education.platform.gameplatform.model.dto.page.PageDto;
import kata.education.platform.gameplatform.model.entity.user.Follower;
import kata.education.platform.gameplatform.model.entity.user.Friend;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.dto.user.UserDtoService;
import kata.education.platform.gameplatform.service.abstracts.model.user.FollowerService;
import kata.education.platform.gameplatform.service.abstracts.model.user.FriendService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Api(value = "Контроллер для работы с сущностью User")
@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserRestController {
    private final FollowerService followerService;
    private final FriendService friendService;
    private final UserService userService;
    private final UserDtoService userDtoService;


    @ApiOperation(value = "Получение пользователя по ID", notes = "Возвращает UserDto по заданному ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully received"),
            @ApiResponse(code = 400, message = "User with id 123 not found!")
    })
    @GetMapping("/{userId}")
    public ResponseEntity<?> getUserById(@PathVariable("userId") Long id) {
        Optional<UserDto> optionalUser = userDtoService.getUserById(id);
        if (optionalUser.isEmpty()) {
            return ResponseEntity.badRequest().body(String.format("User with id " + id + " not found!"));
        }
        return ResponseEntity.ok().body(optionalUser.get());
    }

    @ApiOperation(value = "Подписка на пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Now you are follower with User with id"),
            @ApiResponse(code = 400, message = "Unable to follow user")
    })
    @PostMapping("/{followedUserId}/follow")
    public ResponseEntity<?> followUser(@PathVariable("followedUserId") Long followedUserId) {
        //вытаскивание авторизованного пользователя
        User authentificatedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // проверка на существование пользователя, на которого хотим подписаться
        Optional<User> optionalUser = userService.getById(followedUserId);
        if (optionalUser.isEmpty()) {
            return ResponseEntity.badRequest().body(String.format("User with id " + followedUserId + " not found!"));
        }
        // проверка подписки на самого себя
        if (Objects.equals(followedUserId, authentificatedUser.getId())) {
            return ResponseEntity.badRequest().body("You can't follow yourself");
        }
        // чтобы не дало подписаться, если уже друг
        Optional<Friend> optionalFriend = friendService.findFriendByIds(authentificatedUser.getId(), followedUserId);
        if (optionalFriend.isPresent()) {
            return ResponseEntity.badRequest().body(String.format("You are already friends with User with id "
                    + optionalFriend.get().getFriend().getId()));
        }
        // чтобы не подписался второй раз
        Optional<Follower> optionalFollower = followerService.findFollowerById(followedUserId, authentificatedUser.getId());
        if (optionalFollower.isPresent()) {
            return ResponseEntity.badRequest().body(String.format("You are already a follower of a User with id "
                    + optionalFollower.get().getFollowedUser().getId()));
        }
        // подписка на пользователя
        followerService.followUser(authentificatedUser.getId(), followedUserId);
        return ResponseEntity.ok().body(String.format("Now you are a follower of a User with id " + followedUserId));
    }

    @ApiOperation(value = "Добавление друга")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Now you are friends with User with id"),
            @ApiResponse(code = 400, message = "Unable to add friend")
    })
    @PostMapping("/{followerId}/add")
    public ResponseEntity<?> addFriend(@PathVariable("followerId") Long followerId) {
        // вытаскивание авторизованного пользователя
        User authentificatedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // проверка подписки на самого себя
        if (Objects.equals(followerId, authentificatedUser.getId())) {
            return ResponseEntity.badRequest().body("You can't add yourself");
        }
        // поиск пользователя в таблице подписчиков
        Optional<Follower> follower = followerService.findFollowerById(authentificatedUser.getId(), followerId);
        if (follower.isEmpty()) {
            return ResponseEntity.badRequest().body(String.format("Follower with id " + followerId + " not found!"));
        }
        //добавление друга
        friendService.addFriend(followerId, authentificatedUser.getId());
        return ResponseEntity.ok().body(String.format("Now you are friends with User with id " + followerId));
    }

    @ApiOperation(value = "Получение страницы с листом пользователей",
            notes = "Возвращает PageDto, содержащий в себе лист с UserDto исходя из размеров страницы (size) и номера страницы (page)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully page create"),
            @ApiResponse(code = 400, message = "Invalid page or size")
    })
    @GetMapping(value = "/", params = {"page", "size"})
    public ResponseEntity<PageDto<?>> findPaginated(@RequestParam("page") int currentPage,
                                                    @RequestParam("size") int size) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("itemsOnPage", size);
        parameters.put("currentPage", currentPage);
        PageDto<?> page = userDtoService.getPageDto("userPaginationDaoImpl", parameters);
        return ResponseEntity.ok(page);
    }


    @ApiOperation(value = "Получение данных о текущем пользователе",
                    notes = "Возвращает UserDto ,найденного по id текущего пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request")
    })
    @GetMapping("/currentUser")
    public ResponseEntity<?> getCurrentUser() {
        User authentificatedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = authentificatedUser.getId();
        Optional<UserDto> userDto = userDtoService.getUserById(userId);
        if (userDto.isEmpty()) {
            return ResponseEntity.badRequest().body(String.format("Current user with id " + userId + " not found!"));
        }
        return ResponseEntity.ok().body(userDto.get());
    }
}
