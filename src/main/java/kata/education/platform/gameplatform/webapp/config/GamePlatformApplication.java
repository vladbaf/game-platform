package kata.education.platform.gameplatform.webapp.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@ComponentScan("kata.education.platform.gameplatform")
@EntityScan("kata.education.platform.gameplatform.model.entity")
public class GamePlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamePlatformApplication.class, args);
    }

}
