package kata.education.platform.gameplatform.webapp.controller.rest;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kata.education.platform.gameplatform.exception.UserNotFoundException;
import kata.education.platform.gameplatform.model.dto.page.PageDto;
import kata.education.platform.gameplatform.model.dto.user.UserUpdateDto;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.dto.user.UserDtoService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.webapp.converter.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.ObjLongConsumer;

@Api(value = "Контроллер для работы админа с юзерами")
@RestController
@RequestMapping("/api/admin")
@AllArgsConstructor
public class AdminRestController {

    private final UserService userService;

    private final UserMapper userMapper;
    private final UserDtoService userDtoService;

    @ApiOperation(value="Удаление пользователя по ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User with id was successfully deleted!"),
            @ApiResponse(code = 400, message = "User with id not found!")})
    @DeleteMapping ("delete/{userId}")
    public ResponseEntity<?> deleteUserById(@PathVariable("userId") Long id) {

        Optional<User> optionalUser = userService.getById(id);

        if (optionalUser.isEmpty()) {
            return ResponseEntity.badRequest().body(String.format("User with id %d not found!", id));
        }

        User user = optionalUser.get();

        user.setIsEnabled(false);

        user.setIsDeleted(true);

        userService.update(user);

        return ResponseEntity.ok().body(String.format("User with id %d was successfully deleted!", id));
    }

    @ApiOperation(value = "Получение страницы с листом пользователей",
            notes = "Возвращает PageDto, содержащий в себе лист с UserDto исходя из размеров страницы (size) " +
                    "и номера страницы (page)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Web page created"),
            @ApiResponse(code = 400, message = "Wrong page or size")
    })
    @GetMapping(value = "/users", params = {"page", "size"})
    public ResponseEntity<PageDto<?>> getAllUsers(@RequestParam("page") int currentPage,
                                                  @RequestParam("size") int size) {

        Map<String, Object> settingPage = Map.of("itemsOnPage", size, "currentPage", currentPage);

        return ResponseEntity.ok(userDtoService.getPageDto("userPaginationDaoImpl", settingPage));
    }

    @ApiOperation(value = "Редактирование пользователя по ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User with id was successfully update!"),
            @ApiResponse(code = 400, message = "User with id not found!")
    })
    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody UserUpdateDto userUpdateDto, @PathVariable Long id) {

        if (userService.existsById(id)) {
            userService.update(userMapper.toEntity(userUpdateDto, userService.getById(id).get()));
        } else {
            throw new UserNotFoundException(String.format("User with id %d not found", id));
        }
        return ResponseEntity.ok().body(String.format("User with id %d was successfully update!", id));
    }

}
