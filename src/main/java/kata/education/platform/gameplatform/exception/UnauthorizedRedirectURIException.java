package kata.education.platform.gameplatform.exception;

public class UnauthorizedRedirectURIException extends RuntimeException {

    public UnauthorizedRedirectURIException(){}

    public UnauthorizedRedirectURIException(String message) {
        super(message);
    }
}
