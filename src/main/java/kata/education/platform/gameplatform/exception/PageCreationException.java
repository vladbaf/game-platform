package kata.education.platform.gameplatform.exception;

public class PageCreationException extends IllegalArgumentException {
    public PageCreationException(){}

    public PageCreationException(String message) {
        super(message);
    }
}
