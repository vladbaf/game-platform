package kata.education.platform.gameplatform.exception;


public class FollowerException extends RuntimeException {
    public FollowerException() {
    }


    public FollowerException(String message) {
        super(message);
    }
}