package kata.education.platform.gameplatform.exception;

public class FriendException extends RuntimeException {
    public FriendException() {
    }


    public FriendException(String message) {
        super(message);
    }
}