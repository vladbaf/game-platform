package kata.education.platform.gameplatform.security.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.util.Date;

@Component
public class JwtUtils {

    @Value("${security.jwt-secret-key-b64encoded}")
    private String jwtSecretKeyEncodedString;

    @Value("${security.jwt-expiration-seconds}")
    private long jwtExpiration;

    private Key jwtSecretKey;


    public String generateJwtToken(Authentication authentication) {
        UserDetails details = (UserDetails) authentication.getPrincipal();
        return Jwts.builder().setSubject(details.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() + jwtExpiration * 1000))
                .signWith(jwtSecretKey).compact();
    }

    public String generateJwtToken(String email) {
        return Jwts.builder().setSubject(email)
                .setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() + jwtExpiration * 1000))
                .signWith(jwtSecretKey).compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token);
            return true;
        } catch (MalformedJwtException | IllegalArgumentException e) {
            throw new RuntimeException("JWT token are invalid", e);
        }
    }

    public String getUserLoginFromToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token).getBody().getSubject();
    }


    @PostConstruct
    private void  getSecretKey() {
        byte[] keyBytes = Decoders.BASE64.decode(jwtSecretKeyEncodedString);
        jwtSecretKey = Keys.hmacShaKeyFor(keyBytes);
    }
}
