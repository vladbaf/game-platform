package kata.education.platform.gameplatform.security.oauth.user;

import kata.education.platform.gameplatform.exception.OAuth2AuthenticationProcessingException;
import kata.education.platform.gameplatform.security.oauth.AuthProvider;

import java.util.Map;


public class OAuth2UserInfoFactory {

    public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        switch (AuthProvider.valueOf(registrationId.toUpperCase())) {
            case GOOGLE:
                return new GoogleOAuth2UserInfo(attributes);
                //TODO: TwitterOAuth2UserInfo и FacebookOAuth2UserInfo
            default:
                throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
