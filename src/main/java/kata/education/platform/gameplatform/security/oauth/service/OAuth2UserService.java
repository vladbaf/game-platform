package kata.education.platform.gameplatform.security.oauth.service;

import kata.education.platform.gameplatform.exception.OAuth2AuthenticationProcessingException;
import kata.education.platform.gameplatform.model.dto.request.OAuth2RegisterDto;
import kata.education.platform.gameplatform.model.dto.request.OAuth2RequestDto;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.security.oauth.AuthProvider;
import kata.education.platform.gameplatform.security.oauth.user.OAuth2UserInfo;
import kata.education.platform.gameplatform.security.oauth.user.OAuth2UserInfoFactory;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.webapp.converter.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class OAuth2UserService {

    private final UserService userService;
    private final UserMapper userMapper;
    private final JwtDecoder jwtDecoder;

    public User loadUserFromAccessToken(OAuth2RequestDto oAuth2RequestDto) {
        String token = oAuth2RequestDto.getToken();
        String provider = oAuth2RequestDto.getConnection();
        Jwt jwt = jwtDecoder.decode(token);

        //Значение issuer берется с сайта auth0
        OAuth2TokenValidator<Jwt> validator = JwtValidators.createDefaultWithIssuer("https://dev-y5qq6r8ij277lgbe.us.auth0.com/");
        OAuth2TokenValidatorResult isValid = validator.validate(jwt);
        if (isValid.hasErrors()) {
            throw new OAuth2AuthenticationProcessingException("Token is not valid");
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        String userInfoissuerURI = "https://dev-y5qq6r8ij277lgbe.us.auth0.com/userinfo";
        ResponseEntity<Map> response = restTemplate
                .exchange(userInfoissuerURI, HttpMethod.GET, entity, Map.class);
        OAuth2UserInfo userInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(provider, response.getBody());

        Optional<User> userOptional = userService.getByEmail(userInfo.getEmail());

        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
            updateExistingUser(user, userInfo);
        } else {
            user = registerNewUser(userInfo, provider);
        }
        return user;
    }

    private void updateExistingUser(User existingUser, OAuth2UserInfo userInfo) {
        existingUser.setNickname(userInfo.getName());
        existingUser.setAvatar(userInfo.getImage());
        userService.update(existingUser);
    }

    private User registerNewUser(OAuth2UserInfo userInfo, String provider) {
        OAuth2RegisterDto userToBeRegistered = new OAuth2RegisterDto(
                userInfo.getName(),
                userInfo.getEmail(),
                userInfo.getImage(),
                AuthProvider.valueOf(provider.toUpperCase()));
        User user = userMapper.toEntity(userToBeRegistered);
        userService.persist(user);
        return user;
    }

}
