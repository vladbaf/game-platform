package kata.education.platform.gameplatform.security.oauth;

public enum AuthProvider {
    GOOGLE, FACEBOOK, TWITTER
}
