package kata.education.platform.gameplatform.security;

import kata.education.platform.gameplatform.model.dto.request.AuthenticationRequestDto;
import kata.education.platform.gameplatform.model.dto.response.JwtResponse;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.security.jwt.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    public AuthenticationService(AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    public JwtResponse authenticateAndGetToken(AuthenticationRequestDto authenticationRequestDto) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequestDto.getLogin(),
                    authenticationRequestDto.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtUtils.generateJwtToken(authentication);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            return new JwtResponse(userDetails.getUsername(), token);
        } catch (AuthenticationException authenticationException) {
            throw new BadCredentialsException("Invalid login or password", authenticationException);
        }
    }

    public JwtResponse authenticateOAuth2UserAndGetToken(User user) {
        String email = user.getEmail();
        String token = jwtUtils.generateJwtToken(email);
        return new JwtResponse(email, token);
    }
}
