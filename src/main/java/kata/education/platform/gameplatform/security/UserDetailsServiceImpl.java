package kata.education.platform.gameplatform.security;

import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = userService.getByEmail(email).orElse(null);
        if (user == null) {
            throw new UsernameNotFoundException("User with such e-mail was not found!");
        }
        return user;
    }
}
