package kata.education.platform.gameplatform.service.abstracts.model.chat;

import kata.education.platform.gameplatform.model.entity.chat.SingleChat;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;

public interface SingleChatService extends ReadWriteService<SingleChat, Long> {

    boolean isExistsChatByUsersId(Long userOneId, Long userTwoId);
}
