package kata.education.platform.gameplatform.service.abstracts.dto.pagination;


import kata.education.platform.gameplatform.model.dto.page.PageDto;

import java.util.Map;

public interface PaginationService<T> {
    PageDto<? extends T> getPageDto(String methodName, Map<String, Object> parameters);
}
