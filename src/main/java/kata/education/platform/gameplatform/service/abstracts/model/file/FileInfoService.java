package kata.education.platform.gameplatform.service.abstracts.model.file;

import kata.education.platform.gameplatform.model.entity.file.FileInfo;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;
import org.springframework.web.multipart.MultipartFile;

public interface FileInfoService extends ReadWriteService<FileInfo, Long> {

    String uploadFile(MultipartFile file, User userOwner);

    String generateKey(String name);
}
