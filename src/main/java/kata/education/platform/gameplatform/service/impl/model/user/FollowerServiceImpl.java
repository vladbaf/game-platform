package kata.education.platform.gameplatform.service.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.user.FollowerDao;
import kata.education.platform.gameplatform.model.entity.user.Follower;
import kata.education.platform.gameplatform.service.abstracts.model.user.FollowerService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class FollowerServiceImpl extends ReadWriteServiceImpl<Follower, Long> implements FollowerService {

    private final UserService userService;
    private final FollowerDao followerDao;

    public FollowerServiceImpl(UserService userService, FollowerDao followerDao) {
        super(followerDao);
        this.userService = userService;
        this.followerDao = followerDao;
    }

    @Override
    @Transactional
    public void followUser(Long followerId, Long followedUserIr) {
        Follower follower = new Follower(userService.getById(followerId).get(), userService.getById(followedUserIr).get());
        followerDao.persist(follower);
    }

    @Override
    public Optional<Follower> findFollowerById(Long followedUserId, Long followerId) {
        return followerDao.findFollowerById(followedUserId, followerId);
    }


}
