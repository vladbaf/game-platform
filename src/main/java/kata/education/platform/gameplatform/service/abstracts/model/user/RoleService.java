package kata.education.platform.gameplatform.service.abstracts.model.user;

import kata.education.platform.gameplatform.model.entity.user.Role;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;

import java.util.Optional;

public interface RoleService extends ReadWriteService<Role, Long> {
    Optional<Role> getByName(String name);
}
