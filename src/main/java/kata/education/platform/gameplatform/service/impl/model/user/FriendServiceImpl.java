package kata.education.platform.gameplatform.service.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.user.FollowerDao;
import kata.education.platform.gameplatform.dao.abstracts.model.user.FriendDao;
import kata.education.platform.gameplatform.dao.util.SingleResultUtil;
import kata.education.platform.gameplatform.model.entity.user.Friend;
import kata.education.platform.gameplatform.service.abstracts.model.user.FriendService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.Optional;

@Service
public class FriendServiceImpl extends ReadWriteServiceImpl<Friend, Long> implements FriendService {

    private final FriendDao friendDao;
    private final FollowerDao followerDao;
    private final UserService userService;

    public FriendServiceImpl(FriendDao friendDao, FollowerDao followerDao, UserService userService) {
        super(friendDao);
        this.friendDao = friendDao;
        this.followerDao = followerDao;
        this.userService = userService;
    }


    @Override
    @Transactional
    public void addFriend(Long followerId, Long userId) {
        // добавление в друзья / создание новой сущности в таблицы друзей
        Friend newFriend = new Friend(userService.getById(userId).get(), userService.getById(followerId).get());
        friendDao.persist(newFriend);
        // удаление сущности подписчика
        followerDao.delete(followerDao.findFollowerById(userId, followerId).get());
    }

    @Override
    public Optional<Friend> findFriendByIds(Long userId, Long friendId) {
        return friendDao.findFriendByIds(userId, friendId);
    }


}
