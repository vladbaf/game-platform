package kata.education.platform.gameplatform.service.abstracts.model.post;

import kata.education.platform.gameplatform.model.entity.post.Post;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;

public interface PostService extends ReadWriteService<Post, Long> {

}