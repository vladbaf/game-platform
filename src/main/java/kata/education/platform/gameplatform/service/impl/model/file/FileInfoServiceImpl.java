package kata.education.platform.gameplatform.service.impl.model.file;

import kata.education.platform.gameplatform.dao.abstracts.model.file.FileInfoDao;
import kata.education.platform.gameplatform.model.entity.file.FileInfo;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.file.FileInfoService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.time.LocalDateTime;

@Service
public class FileInfoServiceImpl extends ReadWriteServiceImpl<FileInfo, Long> implements FileInfoService {

    public FileInfoServiceImpl(FileInfoDao fileInfoDao) {
        super(fileInfoDao);
    }

    @Transactional
    @Override
    public String uploadFile(MultipartFile file, User userOwner) {
        String uploadDir = System.getProperty("user.dir") + "\\src\\main\\resources\\uploads\\" + userOwner.getEmail() + "\\";
        String fileName = file.getOriginalFilename();
        FileInfo fileInfo = new FileInfo();
        fileInfo.setKey(generateKey(fileName));
        fileInfo.setName(fileName);
        fileInfo.setUserOwner(userOwner);
        fileInfo.setSize(file.getSize());
        persist(fileInfo);
        try {
            if (!new File(uploadDir).exists()) {
                new File(uploadDir).mkdirs();
            }
            String filePath = uploadDir + fileInfo.getKey() + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
            File dest = new File(filePath);
            file.transferTo(dest);
            return uploadDir;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String generateKey(String name) {
        return DigestUtils.md5Hex(name + LocalDateTime.now().toString());
    }
}
