package kata.education.platform.gameplatform.service.impl.model.user;

import kata.education.platform.gameplatform.dao.abstracts.model.ReadWriteDao;
import kata.education.platform.gameplatform.dao.abstracts.model.user.RoleDao;
import kata.education.platform.gameplatform.model.entity.user.Role;
import kata.education.platform.gameplatform.service.abstracts.model.user.RoleService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class RoleServiceImpl extends ReadWriteServiceImpl<Role, Long> implements RoleService {
    private final RoleDao roleDao;

    public RoleServiceImpl(ReadWriteDao<Role, Long> readWriteDao, RoleDao roleDao) {
        super(readWriteDao);
        this.roleDao = roleDao;
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<Role> getByName(String name) {
        return roleDao.getByName(name);
    }
}
