package kata.education.platform.gameplatform.service.abstracts.model.chat;

import kata.education.platform.gameplatform.model.entity.chat.Message;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;


public interface MessageService extends ReadWriteService<Message, Long> {
}
