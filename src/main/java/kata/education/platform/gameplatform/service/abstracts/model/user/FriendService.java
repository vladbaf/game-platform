package kata.education.platform.gameplatform.service.abstracts.model.user;

import kata.education.platform.gameplatform.model.entity.user.Friend;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;

import java.util.Optional;

public interface FriendService extends ReadWriteService<Friend, Long> {
    void addFriend(Long followerId, Long userId);
    Optional<Friend> findFriendByIds(Long userId, Long friendId);
}
