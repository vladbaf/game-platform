package kata.education.platform.gameplatform.service.impl.model.chat;

import kata.education.platform.gameplatform.dao.abstracts.model.chat.MessageDao;
import kata.education.platform.gameplatform.model.entity.chat.Message;
import kata.education.platform.gameplatform.service.abstracts.model.chat.MessageService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class MessageServiceImpl extends ReadWriteServiceImpl<Message, Long> implements MessageService {

    public MessageServiceImpl(MessageDao messageDao) {
        super(messageDao);
    }
}
