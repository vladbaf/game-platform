package kata.education.platform.gameplatform.service.abstracts.model.user;

import kata.education.platform.gameplatform.model.entity.user.Follower;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;

import java.util.Optional;

public interface FollowerService extends ReadWriteService<Follower, Long> {
    void followUser(Long followerId, Long followedUserId);
    Optional<Follower> findFollowerById(Long followedUserId, Long followerId);
}
