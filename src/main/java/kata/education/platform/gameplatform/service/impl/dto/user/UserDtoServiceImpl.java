package kata.education.platform.gameplatform.service.impl.dto.user;

import kata.education.platform.gameplatform.dao.abstracts.dto.user.UserDtoDao;
import kata.education.platform.gameplatform.model.dto.user.UserDto;
import kata.education.platform.gameplatform.service.abstracts.dto.user.UserDtoService;
import kata.education.platform.gameplatform.service.impl.dto.pagination.PaginationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDtoServiceImpl extends PaginationServiceImpl<UserDto> implements UserDtoService {

    private final UserDtoDao userDtoDao;

    @Autowired
    public UserDtoServiceImpl(UserDtoDao userDtoDao) {
        this.userDtoDao = userDtoDao;
    }

    @Override
    public Optional<UserDto> getUserById(Long id) {
        return userDtoDao.getUserById(id);
    }
}
