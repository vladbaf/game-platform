package kata.education.platform.gameplatform.service.impl.dto.pagination;

import kata.education.platform.gameplatform.dao.abstracts.dto.pagination.PaginationDao;
import kata.education.platform.gameplatform.exception.PageCreationException;
import kata.education.platform.gameplatform.model.dto.page.PageDto;
import kata.education.platform.gameplatform.service.abstracts.dto.pagination.PaginationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PaginationServiceImpl<T> implements PaginationService<Object> {
    @Autowired
    Map<String, PaginationDao<T>> paginationDaos;

    @Override
    public PageDto<? extends T> getPageDto(String methodName, Map<String, Object> parameters) {
        PaginationDao<T> paginationDao = paginationDaos.get(methodName);
        int currentPage = (int) parameters.get("currentPage");
        int itemsOnPage = (int) parameters.get("itemsOnPage");

        if (currentPage <= 0 || itemsOnPage <= 0) {
            throw new PageCreationException("Unable to create page: currentPage or itemsOnPage <= 0");
        }

        return new PageDto<>(currentPage,
                             itemsOnPage,
                             paginationDao.getItemsCount(parameters),
                             paginationDao.getItems(parameters));
    }
}
