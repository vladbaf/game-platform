
package kata.education.platform.gameplatform.service.impl.model.post;

import kata.education.platform.gameplatform.dao.abstracts.model.post.PostDao;
import kata.education.platform.gameplatform.model.entity.post.Post;
import kata.education.platform.gameplatform.service.abstracts.model.post.PostService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl extends ReadWriteServiceImpl<Post, Long> implements PostService {
    public PostServiceImpl(PostDao postDao) {
        super(postDao);
    }
}