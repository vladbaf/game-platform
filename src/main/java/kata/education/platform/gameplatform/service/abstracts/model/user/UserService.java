package kata.education.platform.gameplatform.service.abstracts.model.user;

import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.ReadWriteService;

import java.util.Optional;

public interface UserService extends ReadWriteService<User, Long> {

    Optional<User> getByEmail(String email);

    boolean existsByEmail(String email);

}
