package kata.education.platform.gameplatform.service.impl.model.chat;

import kata.education.platform.gameplatform.dao.abstracts.model.chat.SingleChatDao;
import kata.education.platform.gameplatform.model.entity.chat.SingleChat;
import kata.education.platform.gameplatform.service.abstracts.model.chat.SingleChatService;
import kata.education.platform.gameplatform.service.impl.model.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class SingleChatServiceImpl extends ReadWriteServiceImpl<SingleChat, Long> implements SingleChatService {
    private final SingleChatDao singleChatDao;

    public SingleChatServiceImpl(SingleChatDao singleChatDao, SingleChatDao singleChatDao1) {
        super(singleChatDao);
        this.singleChatDao = singleChatDao1;
    }

    @Override
    public boolean isExistsChatByUsersId(Long userOneId, Long userTwoId) {
        return singleChatDao.isExistsChatByUsersId(userOneId, userTwoId);
    }
}
