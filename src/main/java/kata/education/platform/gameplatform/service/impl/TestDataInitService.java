package kata.education.platform.gameplatform.service.impl;

import kata.education.platform.gameplatform.model.entity.user.Role;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.user.RoleService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TestDataInitService {

    private final UserService userService;
    private final RoleService roleService;

    private final PasswordEncoder passwordEncoder;
    private static Role adminRole;
    private static Role userRole;

    private final static int usersQuantity;
    private final static List<String> nicknames;
    private final static List<String> emailDomains;


    /**
     * Класс с инициализацией базы данных пользователями. Создаёт в бд одного админа и заданное количество пользователей.
     */
    public TestDataInitService(UserService userService, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    public void addDefaultDataIntoDb() {
        addRolesData();
        addUsersData();
    }

    private void addRolesData() {
        adminRole = new Role("ROLE_ADMIN");
        userRole = new Role("ROLE_USER");
        roleService.persistAll(adminRole, userRole);
    }

    private void addUsersData() {
        List<User> userList = new ArrayList<>();
        User admin = new User();
        admin.setNickname("admin");
        admin.setRole(adminRole);
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setEmail("admin@mail.com");
        userList.add(admin);
        Collections.shuffle(nicknames);
        Random random = new Random();
        nicknames.stream().map(nickname -> {
            User user = new User();
            user.setNickname(nickname);
            user.setRole(userRole);
            user.setPassword(passwordEncoder.encode("user"));
            user.setEmail(nickname.toLowerCase().replaceAll("\\s", "") + emailDomains.get(random.nextInt(emailDomains.size())));
            return user;
        }).limit(usersQuantity).forEach(userList::add);
        userService.persistAll(userList);
    }

    static {
        usersQuantity = 25;
        emailDomains = new ArrayList<>(Arrays.asList("@gmail.com", "@yahoo.com", "@hotmail.com", "@aol.com", "@hotmail.co.uk", "@hotmail.fr", "@msn.com", "@yahoo.fr", "@wanadoo.fr", "@orange.fr", "@comcast.net", "@yahoo.co.uk", "@yahoo.com.br", "@yahoo.co.in", "@live.com", "@rediffmail.com", "@free.fr", "@gmx.de", "@web.de", "@yandex.ru", "@ymail.com", "@libero.it", "@outlook.com", "@uol.com.br", "@mail.ru", "@cox.net", "@hotmail.it"));
        nicknames = new ArrayList<>(Arrays.asList("Aspect", "Kraken", "Bender", "Lynch", "Big Papa", "Mad Dog", "Bowser", "Bruise", "Psycho", "Cannon", "Ranger", "Clink", "Ratchet", "Cobra", "Reaper", "Colt", "Rigs", "Crank", "Ripley", "Creep", "Roadkill", "Daemon", "Ronin", "Decay", "Rubble", "Diablo", "Sasquatch", "Doom", "Scar", "Dracula", "Shiver", "Dragon", "Skinner", "Fender", "Skull Crusher", "Fester", "Slasher", "Fisheye", "Steelshot", "Flack", "Surge", "Gargoyle", "Sythe", "Grave", "Trip", "Gunner", "Trooper", "Hash", "Tweek", "Hashtag", "Vein", "Indominus", "Void", "Ironclad", "Wardon", "Killer", "Wraith", "Knuckles", "Zero", "Steel", "Kevlar", "Lightning", "Tito", "Bullet-Proof", "Fire-Bred", "Titanium", "Hurricane", "Ironsides", "Iron-Cut", "Tempest", "Iron Heart", "Steel Forge", "Pursuit", "Steel Foil", "Sick Rebellious Names", "Upsurge", "Uprising", "Overthrow", "Breaker", "Sabotage", "Dissent", "Subversion", "Rebellion", "Insurgent", "Accidental Genius", "Acid Gosling", "Admiral Tot", "AgentHercules", "Airport Hobo", "Alley Frog", "Alpha", "AlphaReturns", "Angel", "AngelsCreed", "Arsenic Coo", "Atomic Blastoid", "Automatic Slicer", "Baby Brown", "Back Bett", "Bad Bunny", "Bazooka Har-de-har", "Bearded Angler", "Beetle King", "Betty Cricket", "Bit Sentinel", "Bitmap", "BlacKitten", "Blister", "Blistered Outlaw", "Blitz", "BloodEater", "Bonzai", "BoomBeachLuvr", "BoomBlaster", "Bootleg Taximan", "Bowie", "Bowler", "Breadmaker", "Broomspun", "Chew Chew", "Ballistic", "Furore", "Uproar", "Fury", "Ire", "Demented", "Wrath", "Madness", "Schizo", "Rage", "Savage", "Manic", "Frenzy", "Mania", "Derange", "CobraBite", "Cocktail", "CollaterolDamage", "CommandX", "Commando", "Congo Wire", "Cool Iris", "Cool Whip", "Cosmo", "Crash Override", "Crash Test", "Crazy Eights", "Criss Cross", "Cross Thread", "Cujo", "Cupid Dust", "Daffy Girl", "Dahlia Bumble", "DaisyCraft", "Dancing Madman", "Dangle", "DanimalDaze", "Dark Horse", "Darkside Orbit", "Darling Peacock", "Day Hawk", "Desert Haze", "Desperado", "Devil Blade", "Devil Chick", "Dexter", "Diamond Gamer", "Digger", "Disco Potato", "Disco Thunder", "DiscoMate", "Don Stab", "Doz Killer", "Dredd", "DriftDetector", "DriftManiac", "Drop Stone", "Dropkick", "Drugstore Cowboy", "DuckDuck", "Earl of Arms", "Easy Sweep", "Eerie Mizzen", "ElactixNova", "Elder Pogue", "Electric Player", "Electric Saturn", "Ember Rope", "Esquire", "ExoticAlpha", "EyeShooter", "Fabulous", "Fast Draw", "FastLane", "Father Abbot", "FenderBoyXXX", "Fennel Dove", "Feral Mayhem", "Fiend Oblivion", "FifthHarmony", "Fire Feline", "Fire Fish", "FireByMisFire", "Fist Wizard", "Atilla", "Darko", "Terminator", "Conqueror", "Mad Max", "Siddhartha", "Suleiman", "Billy the Butcher", "Thor", "Napoleon", "Maximus", "Khan", "Geronimo", "Leon", "Leonidas"));
    }
}
