package kata.education.platform.gameplatform.service.abstracts.dto.user;

import kata.education.platform.gameplatform.model.dto.user.UserDto;
import kata.education.platform.gameplatform.service.abstracts.dto.pagination.PaginationService;

import java.util.Optional;

public interface UserDtoService extends PaginationService<Object> {
    Optional<UserDto> getUserById(Long id);
}
