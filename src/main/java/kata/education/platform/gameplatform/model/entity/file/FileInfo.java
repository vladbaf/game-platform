package kata.education.platform.gameplatform.model.entity.file;

import kata.education.platform.gameplatform.model.entity.user.User;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "files")
public class FileInfo {
    @Id
    @GeneratedValue(generator = "file_seq")
    private Long id;

    @Column
    @NonNull
    private String name;

    @Column
    @NonNull
    private Long size;

    @Column
    @NonNull
    private String key;


    @Column(name = "persist_date", updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @CreationTimestamp
    private LocalDateTime persistDate;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false)
    @NonNull
    private User userOwner;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo file = (FileInfo) o;
        return Objects.equals(id, file.id)
                && name.equals(file.name)
                && size.equals(file.size)
                && key.equals(file.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, size, key);
    }

}
