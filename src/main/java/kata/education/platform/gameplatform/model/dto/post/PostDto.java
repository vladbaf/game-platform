package kata.education.platform.gameplatform.model.dto.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "DTO for post presentation")
public class PostDto {
    @NotNull
    @ApiModelProperty(
            notes = "Post id",
            name = "id",
            dataType = "Long",
            example = "3")
    Long id;

    @NotNull
    @ApiModelProperty(
            notes = "Title of the post",
            name = "Title",
            dataType = "String",
            example = "New post")
    String title;

    @NotNull
    @ApiModelProperty(
            notes = "Text of the post",
            name = "Text",
            dataType = "String",
            example = "Some strings in post. Now you can see post text")
    String text;

    @NonNull
    @ApiModelProperty(
            notes = "User id",
            name = "userId",
            dataType = "String",
            example = "10")
    String userId;
    @ApiModelProperty(
            notes = "User nickname",
            name = "Nickname",
            dataType = "String",
            example = "Mister admin")
    String nickname;

    @ApiModelProperty(
            notes = "Date of post creating",
            name = "persistDate",
            dataType = "LocalDateTime",
            example = "2023-02-07T16:12:56.2164912")
    LocalDateTime persistDate;
}
