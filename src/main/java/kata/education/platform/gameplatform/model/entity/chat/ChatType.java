package kata.education.platform.gameplatform.model.entity.chat;

public enum ChatType {
    SINGLE_CHAT,
    GROUP_CHAT
}
