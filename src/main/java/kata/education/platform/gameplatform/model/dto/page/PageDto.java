package kata.education.platform.gameplatform.model.dto.page;

import kata.education.platform.gameplatform.exception.PageCreationException;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PageDto<T> {
    private int currentPage;
    private int totalPages;
    private int itemsOnPage;
    Long totalItemsCount;
    List<T> items;

    public PageDto(int currentPage, int itemsOnPage, Long totalItemsCount, List<T> items) {
        this.currentPage = currentPage;
        this.itemsOnPage = itemsOnPage;
        this.totalItemsCount = totalItemsCount;
        this.items = items;

        if (totalItemsCount == 0) {
            throw new PageCreationException("No items found");
        } else {
            this.totalPages = (totalItemsCount % itemsOnPage == 0) ?
                    (int) (totalItemsCount / itemsOnPage) : (int) (totalItemsCount / itemsOnPage) + 1;
        }
        if (currentPage > this.totalPages) {
            throw new PageCreationException("Current page is out of range: check your parameters");
        }
    }
}