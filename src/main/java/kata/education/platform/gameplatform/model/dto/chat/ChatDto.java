package kata.education.platform.gameplatform.model.dto.chat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatDto {
    Long id;
    String title;
    String type;
    String image;
    String lastMessage;
}
