package kata.education.platform.gameplatform.model.entity.post;


import kata.education.platform.gameplatform.model.entity.user.User;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "post")
public class Post {


    @Id
    @GeneratedValue(generator = "post_seq")
    private Long id;

    @Column(length = 50)
    @NonNull
    private String title;

    @Column(length = 1000)
    @NonNull
    private String text;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = "user_id")
    private User user;


    @Column(name = "persist_date", updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @CreationTimestamp
    private LocalDateTime persistDate;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id)
                && title.equals(post.title)
                && text.equals(post.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, text);
    }
}
