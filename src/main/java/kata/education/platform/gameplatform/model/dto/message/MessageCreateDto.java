package kata.education.platform.gameplatform.model.dto.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageCreateDto {

    private Long chatId;
    private Long userId;
    private String message;
}
