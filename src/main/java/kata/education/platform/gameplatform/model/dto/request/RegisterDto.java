package kata.education.platform.gameplatform.model.dto.request;

import lombok.*;

@Data
@AllArgsConstructor
public class RegisterDto {
    private String nickname;
    private String email;
    private String password;
}
