package kata.education.platform.gameplatform.model.entity.chat;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@ToString
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "chat")
public class Chat {

    @Id
    @GeneratedValue(generator = "chat_seq")
    private Long id;

    @Column(name = "persist_date", updatable = false)
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @CreationTimestamp
    private LocalDateTime persistDate;


    @OneToMany(mappedBy = "chat", fetch = FetchType.LAZY, targetEntity = Message.class, cascade = {CascadeType.REMOVE})
    @NonNull
    private Set<Message> messages;

    @Column
    @NonNull
    private String image;

    @Column
    @NonNull
    private String title;

    @Column
    @NonNull
    private ChatType type;

    public Chat(ChatType type) {
        this.type = type;
    }
}

