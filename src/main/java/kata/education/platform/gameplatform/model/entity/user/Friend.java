package kata.education.platform.gameplatform.model.entity.user;

import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "friends")
public class Friend {
    @Id
    @GeneratedValue(generator = "Friend_seq")
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false)
    @NonNull
    private User user;  // сам пользователь
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "friend_id", nullable = false)
    @NonNull
    private User friend;    // друг пользователя

    public Friend(@NonNull User user, @NonNull User friend) {
        this.user = user;
        this.friend = friend;
    }
}
