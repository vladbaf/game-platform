package kata.education.platform.gameplatform.model.entity.user;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "followers")
public class Follower {
    @Id
    @GeneratedValue(generator = "Follower_seq")
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "follower_id", nullable = false)
    @NonNull
    private User follower; // сам пользователь
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "followed_user_id", nullable = false)
    @NonNull
    private User followedUser;  // тот, на кого подписывается пользователь

    public Follower(@NonNull User follower, @NonNull User followedUser) {
        this.follower = follower;
        this.followedUser = followedUser;
    }

}
