package kata.education.platform.gameplatform.model.entity.chat;

import kata.education.platform.gameplatform.model.entity.user.User;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String message;

    @Column(name = "persist_date", updatable = false)
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @CreationTimestamp
    private LocalDateTime persistDate;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Chat.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "chat_id", nullable = false)
    @NonNull
    private Chat chat;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false)
    @NonNull
    private User userSender;
}
