package kata.education.platform.gameplatform.model.entity.chat;

import kata.education.platform.gameplatform.model.entity.user.User;
import lombok.*;

import javax.persistence.*;

@ToString
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "single_chat")
public class SingleChat {

    @Id
    @GeneratedValue(generator = "single_chat_seq")
    private Long id;

    @OneToOne
    @MapsId
    @NonNull
    private Chat chat = new Chat(ChatType.SINGLE_CHAT);

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_one_id", nullable = false)
    @NonNull
    private User userOne;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_two_id", nullable = false)
    @NonNull
    private User userTwo;


}
