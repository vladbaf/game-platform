package kata.education.platform.gameplatform.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OAuth2RequestDto {
    private String token;
    private String connection;
}
