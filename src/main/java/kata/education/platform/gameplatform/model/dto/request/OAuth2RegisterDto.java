package kata.education.platform.gameplatform.model.dto.request;

import kata.education.platform.gameplatform.security.oauth.AuthProvider;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OAuth2RegisterDto {

    private String nickname;
    private String email;
    private String avatar;
    private AuthProvider provider;


}
