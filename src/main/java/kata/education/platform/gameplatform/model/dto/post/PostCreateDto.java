package kata.education.platform.gameplatform.model.dto.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "DTO for post creating")
public class PostCreateDto {
    @NotNull(message = "Title is empty")
    @ApiModelProperty(
            notes = "Title of the post",
            name = "Title",
            dataType = "String",
            example = "New post")
    String title;
    @NotNull(message = "Text is empty")
    @ApiModelProperty(
            notes = "Text of the post",
            name = "Text",
            dataType = "String",
            example = "Some strings in post. Now you can create a post")
    String text;
    @NotNull(message = "Id is empty")
    @ApiModelProperty(
            notes = "User id",
            name = "userId",
            dataType = "Long",
            example = "10")
    Long userId;
}
