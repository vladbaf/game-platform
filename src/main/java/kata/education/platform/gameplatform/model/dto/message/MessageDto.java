package kata.education.platform.gameplatform.model.dto.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

    private Long id;
    private Long chatId;
    private Long userId;
    private String userAvatar;
    private String message;
    private LocalDateTime persistDate;
    private LocalDateTime lastRedactionDate;

}
