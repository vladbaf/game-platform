package kata.education.platform.gameplatform.tests.api;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.SeedStrategy;
import kata.education.platform.gameplatform.model.entity.user.Follower;
import kata.education.platform.gameplatform.model.entity.user.Friend;
import kata.education.platform.gameplatform.tests.config.AbstractSpringTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class UserRestControllerTest extends AbstractSpringTest {

    String jwtToken;
    @Autowired
    EntityManager entityManager;

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml",
            "dataset/user_rest_controller_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void getUserById() throws Exception {
        jwtToken = getJwtToken("user100@mail.ru", "111");
        mvc.perform(get("/api/user/{userId}", 101)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("101"))
                .andExpect(jsonPath("$.nickname").value("user101"))
                .andExpect(jsonPath("$.coins").value(300))
                .andExpect(jsonPath("$.avatar").value("some/string/with/path/avatar.jpg"));


        mvc.perform(get("/api/user/{userId}", 106)
                        .header("Authorization", jwtToken)
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().string("User with id 106 not found!"));
    }

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml",
            "dataset/user_rest_controller_dataset/role.yml",
            "dataset/user_rest_controller_dataset/followers.yml",
            "dataset/user_rest_controller_dataset/friends.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void followUser() throws Exception {
        //вытаскивание авторизованного пользователя
        jwtToken = getJwtToken("user100@mail.ru", "111");

        //подписка на несуществуеющего пользователя
        mvc.perform(post("/api/user/{followedUserId}/follow", 106)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("User with id 106 not found!"));

        //подписка на себя
        mvc.perform(post("/api/user/{followedUserId}/follow", 100)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("You can't follow yourself"));

        //повторная подписка на пользователя
        mvc.perform(post("/api/user/{followedUserId}/follow", 101)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("You are already a follower of a User with id 101"));

        //подписка на друга
        mvc.perform(post("/api/user/{followedUserId}/follow", 102)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("You are already friends with User with id 102"));

        //подписка на существуеющего пользователя
        mvc.perform(post("/api/user/{followedUserId}/follow", 103)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("Now you are a follower of a User with id 103"));

        Assertions.assertEquals(112L, (long) entityManager
                .createQuery("FROM Follower e WHERE e.followedUser.id = :followedUserId " +
                        "and  e.follower.id = :followerId", Follower.class)
                .setParameter("followedUserId", 104L)
                .setParameter("followerId", 100L)
                .getSingleResult()
                .getId());
    }

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml",
            "dataset/user_rest_controller_dataset/role.yml",
            "dataset/user_rest_controller_dataset/followers.yml",
            "dataset/user_rest_controller_dataset/friends.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void addFriend() throws Exception {
        jwtToken = getJwtToken("user100@mail.ru", "111");

        // добавление себя
        mvc.perform(post("/api/user/{followedUserId}/add", 100)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("You can't add yourself"));

        // попытка добавить не фолловера
        mvc.perform(post("/api/user/{followerId}/add", 102)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("Follower with id 102 not found!"));

        // добавление в друзья
        mvc.perform(post("/api/user/{followerId}/add", 103)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk());

        Assertions.assertEquals(120L, (long) entityManager
                .createQuery("FROM Friend e WHERE e.user.id = :userId and e.friend.id = :friendId", Friend.class)
                .setParameter("userId", 100L)
                .setParameter("friendId", 102L)
                .getSingleResult()
                .getId());
    }

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml", "dataset/user_rest_controller_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void findPaginated() throws Exception {
        jwtToken = getJwtToken("user100@mail.ru", "111");
        mvc.perform(get("/api/user/?page={page}&size={size}", 1, 2)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentPage").value("1"))
                .andExpect(jsonPath("$.totalPages").value("3"))
                .andExpect(jsonPath("$.itemsOnPage").value("2"))
                .andExpect(jsonPath("$.totalItemsCount").value("6"))
                .andExpect(jsonPath("$.items[0].id").value("100"))
                .andExpect(jsonPath("$.items[0].nickname").value("user100"))
                .andExpect(jsonPath("$.items[0].coins").value("250"))
                .andExpect(jsonPath("$.items[0].avatar").value("some/string/with/path/avatar1.jpg"))
                .andExpect(jsonPath("$.items[1].id").value("101"))
                .andExpect(jsonPath("$.items[1].nickname").value("user101"))
                .andExpect(jsonPath("$.items[1].coins").value("300"))
                .andExpect(jsonPath("$.items[1].avatar").value("some/string/with/path/avatar.jpg"))
                .andExpect(jsonPath("$.items[2]").doesNotExist())
                .andExpect(status().isOk());

        mvc.perform(get("/api/user/?page={page}&size={size}", 2, 3)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentPage").value("2"))
                .andExpect(jsonPath("$.totalPages").value("2"))
                .andExpect(jsonPath("$.itemsOnPage").value("3"))
                .andExpect(jsonPath("$.totalItemsCount").value("6"))
                .andExpect(jsonPath("$.items[0].id").value("103"))
                .andExpect(jsonPath("$.items[0].nickname").value("user103"))
                .andExpect(jsonPath("$.items[0].coins").value("310"))
                .andExpect(jsonPath("$.items[0].avatar").value("some/string/with/path/avatar3.jpg"))
                .andExpect(jsonPath("$.items[1].id").value("104"))
                .andExpect(jsonPath("$.items[1].nickname").value("user104"))
                .andExpect(jsonPath("$.items[1].coins").value("350"))
                .andExpect(jsonPath("$.items[1].avatar").value("some/string/with/path/avatar4.jpg"))
                .andExpect(jsonPath("$.items[2].id").value("105"))
                .andExpect(jsonPath("$.items[2].nickname").value("user105"))
                .andExpect(jsonPath("$.items[2].coins").value("3000"))
                .andExpect(jsonPath("$.items[2].avatar").value("some/string/with/path/avatar5.jpg"))
                .andExpect(jsonPath("$.items[3]").doesNotExist())
                .andExpect(status().isOk());

        mvc.perform(get("/api/user/?page={page}&size={size}", 0, 2)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Unable to create page: currentPage or itemsOnPage <= 0"));

        mvc.perform(get("/api/user/?page={page}&size={size}", 3, -1)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Unable to create page: currentPage or itemsOnPage <= 0"));

        mvc.perform(get("/api/user/?page={page}&size={size}", 4, 2)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Current page is out of range: check your parameters"));
    }

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml",
            "dataset/user_rest_controller_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void getCurrentUser() throws Exception {
        //вытаскивание авторизованного пользователя
        jwtToken = getJwtToken("user100@mail.ru", "111");

        mvc.perform(get("/api/user/currentUser", 100)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("100"))
                .andExpect(jsonPath("$.nickname").value("user100"))
                .andExpect(jsonPath("$.coins").value(250))
                .andExpect(jsonPath("$.avatar").value("some/string/with/path/avatar1.jpg"));
    }
}
