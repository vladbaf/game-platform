package kata.education.platform.gameplatform.tests.api;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.SeedStrategy;
import kata.education.platform.gameplatform.model.dto.chat.ChatDto;
import kata.education.platform.gameplatform.model.entity.chat.Chat;
import kata.education.platform.gameplatform.model.entity.chat.Message;
import kata.education.platform.gameplatform.model.entity.chat.SingleChat;
import kata.education.platform.gameplatform.tests.config.AbstractSpringTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ChatRestControllerTest extends AbstractSpringTest {
    protected String jwtToken;

    @Autowired
    EntityManager entityManager;

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml", "dataset/user_rest_controller_dataset/role.yml"}, strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void createNewSingleChatTest() throws Exception {
        ChatDto chatDto = new ChatDto(null, "My Friend", "SINGLE_CHAT", "Image", "Hello");
        jwtToken = getJwtToken("user100@mail.ru", "111");
        mvc.perform(get("/api/chats/single-chats/user/{userId}", 102)
                        .header("Authorization", jwtToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(chatDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("My Friend"))
                .andExpect(jsonPath("$.type").value("SINGLE_CHAT"))
                .andExpect(jsonPath("$.image").value("Image"))
                .andExpect(jsonPath("$.lastMessage").value("Hello"));

        Assertions.assertEquals(1L, (long) entityManager
                .createQuery("FROM SingleChat e WHERE e.userOne.id = :userOneId and e.userTwo.id = :userTwoId", SingleChat.class)
                .setParameter("userOneId", 100L)
                .setParameter("userTwoId", 102L)
                .getSingleResult().getId());

        Assertions.assertEquals("Hello",  entityManager
                .createQuery("FROM Message e WHERE e.chat.id = :chatId and e.userSender.id = :userId", Message.class)
                .setParameter("chatId", 1L)
                .setParameter("userId", 100L)
                .getSingleResult()
                .getMessage());

        Assertions.assertEquals("My Friend",  entityManager
                .createQuery("FROM Chat e WHERE e.id = :Id", Chat.class)
                .setParameter("Id", 1L)
                .getSingleResult()
                .getTitle());

        mvc.perform(get("/api/chats/single-chats/user/{userId}", 102)
                        .header("Authorization", jwtToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(chatDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("You can't create an existing chat"));

        mvc.perform(get("/api/chats/single-chats/user/{userId}", 999)
                        .header("Authorization", jwtToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(chatDto)))
                .andDo(print())
                .andExpect(jsonPath("$").value("User with id 999 not found!"));

        mvc.perform(get("/api/chats/single-chats/user/{userId}", 100)
                        .header("Authorization", jwtToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(chatDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("You can't create a chat with yourself"));
    }
}
