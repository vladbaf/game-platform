package kata.education.platform.gameplatform.tests.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.junit5.api.DBRider;
import kata.education.platform.gameplatform.model.dto.request.AuthenticationRequestDto;
import kata.education.platform.gameplatform.model.dto.response.JwtResponse;
import kata.education.platform.gameplatform.webapp.config.GamePlatformApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@DBRider
@SpringBootTest(classes = GamePlatformApplication.class)
@TestPropertySource(properties = "spring.config.location=src/test/resources/application-test.properties")
@DBUnit(caseSensitiveTableNames = true, cacheConnection = false, allowEmptyFields = true)
@AutoConfigureMockMvc
public abstract class AbstractSpringTest {
    @Autowired
    public MockMvc mvc;

    @Autowired
    protected EntityManager em;

    @Autowired
    protected ObjectMapper objectMapper;


    public String getJwtToken(String login, String password) throws Exception {
        AuthenticationRequestDto request = new AuthenticationRequestDto();
        request.setLogin(login);
        request.setPassword(password);
        MvcResult m = this.mvc.perform(post("/api/auth/token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andReturn();
        JwtResponse jwtResponse = objectMapper.readValue(m.getResponse().getContentAsString(), JwtResponse.class);
        return "Bearer " + jwtResponse.getToken();
    }

}
