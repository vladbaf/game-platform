package kata.education.platform.gameplatform.tests.api;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.SeedStrategy;
import kata.education.platform.gameplatform.model.dto.post.PostCreateDto;
import kata.education.platform.gameplatform.tests.config.AbstractSpringTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PostRestControllerTest extends AbstractSpringTest {
    String jwtToken;

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml",
            "dataset/user_rest_controller_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void createPost() throws Exception {
        jwtToken = getJwtToken("user100@mail.ru", "111");
        PostCreateDto postCreateDto = new PostCreateDto("testTitle","testText",102L);
        PostCreateDto postCreateDto2 = new PostCreateDto("anotherTitle", "anotherText", 103L);

        mvc.perform(post("/api/post/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", jwtToken)
                        .content(objectMapper.writeValueAsString(postCreateDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.title").value("testTitle"))
                .andExpect(jsonPath("$.text").value("testText"))
                .andExpect(jsonPath("$.userId").value(102))
                .andExpect(jsonPath("$.nickname").value("user102"));

        mvc.perform(post("/api/post/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", jwtToken)
                        .content(objectMapper.writeValueAsString(postCreateDto2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.title").value("anotherTitle"))
                .andExpect(jsonPath("$.text").value("anotherText"))
                .andExpect(jsonPath("$.userId").value(103))
                .andExpect(jsonPath("$.nickname").value("user103"));
    }

}