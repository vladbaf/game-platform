package kata.education.platform.gameplatform.tests.api;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.SeedStrategy;
import kata.education.platform.gameplatform.exception.UserNotFoundException;
import kata.education.platform.gameplatform.model.dto.user.UserUpdateDto;
import kata.education.platform.gameplatform.model.entity.user.Role;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.service.abstracts.model.user.RoleService;
import kata.education.platform.gameplatform.service.abstracts.model.user.UserService;
import kata.education.platform.gameplatform.tests.config.AbstractSpringTest;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.MediaType;

import javax.management.relation.RoleNotFoundException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class AdminRestControllerTest extends AbstractSpringTest {

    String jwtToken;

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml",
            "dataset/user_rest_controller_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void deleteUserByIdTest() throws Exception {

        jwtToken = getJwtToken("user100@mail.ru", "111");

        mvc.perform(delete("/api/admin/delete/{userId}", 106)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("User with id 106 not found!"));

        mvc.perform(delete("/api/admin/delete/{userId}", 101)
                        .header("Authorization", jwtToken)
                       )
                .andExpect(status().isOk())
                .andReturn();

        Assertions.assertEquals(true ,em
                .createQuery("FROM User WHERE isDeleted = true", User.class)
                .getSingleResult()
                .getIsDeleted());

        Assertions.assertEquals(false,  em
                .createQuery("FROM User WHERE isEnabled = false", User.class)
                .getSingleResult()
                .getIsEnabled());
    }

    @Test
    @DataSet(value = {"dataset/admin_rest_controller_dataset/users.yml", "dataset/admin_rest_controller_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void getAllUserTest() throws Exception {
        jwtToken = getJwtToken("admin@mail.ru", "111");

        mvc.perform(get("/api/admin/users/?page={page}&size={size}", 1, 2)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentPage").value("1"))
                .andExpect(jsonPath("$.totalPages").value("3"))
                .andExpect(jsonPath("$.itemsOnPage").value("2"))
                .andExpect(jsonPath("$.totalItemsCount").value("6"))

                .andExpect(jsonPath("$.items[0].id").value("1"))
                .andExpect(jsonPath("$.items[0].nickname").value("admin"))
                .andExpect(jsonPath("$.items[0].coins").value("1000"))
                .andExpect(jsonPath("$.items[0].avatar").value("some/string/with/path/avatar1.jpg"))

                .andExpect(jsonPath("$.items[1].id").value("2"))
                .andExpect(jsonPath("$.items[1].nickname").value("Max"))
                .andExpect(jsonPath("$.items[1].coins").value("200"))
                .andExpect(jsonPath("$.items[1].avatar").value("some/string/with/path/avatar.jpg"))
                .andExpect(jsonPath("$.items[2]").doesNotExist())
                .andExpect(status().isOk());

        mvc.perform(get("/api/admin/users/?page={page}&size={size}", 2, 3)
                        .header("Authorization", jwtToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currentPage").value("2"))
                .andExpect(jsonPath("$.totalPages").value("2"))
                .andExpect(jsonPath("$.itemsOnPage").value("3"))
                .andExpect(jsonPath("$.totalItemsCount").value("6"))

                .andExpect(jsonPath("$.items[0].id").value("4"))
                .andExpect(jsonPath("$.items[0].nickname").value("Anna"))
                .andExpect(jsonPath("$.items[0].coins").value("400"))
                .andExpect(jsonPath("$.items[0].avatar").value("some/string/with/path/avatar3.jpg"))

                .andExpect(jsonPath("$.items[1].id").value("5"))
                .andExpect(jsonPath("$.items[1].nickname").value("Egor"))
                .andExpect(jsonPath("$.items[1].coins").value("500"))
                .andExpect(jsonPath("$.items[1].avatar").value("some/string/with/path/avatar4.jpg"))

                .andExpect(jsonPath("$.items[2].id").value("6"))
                .andExpect(jsonPath("$.items[2].nickname").value("Dima"))
                .andExpect(jsonPath("$.items[2].coins").value("600"))
                .andExpect(jsonPath("$.items[2].avatar").value("some/string/with/path/avatar5.jpg"))
                .andExpect(jsonPath("$.items[3]").doesNotExist())
                .andExpect(status().isOk());

        mvc.perform(get("/api/admin/users/?page={page}&size={size}", 0, 2)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Unable to create page: currentPage or itemsOnPage <= 0"));

        mvc.perform(get("/api/admin/users/?page={page}&size={size}", 3, -1)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Unable to create page: currentPage or itemsOnPage <= 0"));

        mvc.perform(get("/api/admin/users/?page={page}&size={size}", 4, 2)
                        .header("Authorization", jwtToken))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Current page is out of range: check your parameters"));
    }

    @Test
    @DataSet(value = {"dataset/admin_rest_controller_edit_user_dataset/users.yml",
            "dataset/admin_rest_controller_edit_user_dataset/role.yml"},
            strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void updateUserTest() throws Exception {

        jwtToken = getJwtToken("user2@mail.ru", "111");

        UserUpdateDto userDto = new UserUpdateDto();
        userDto.setId(1L);
        userDto.setEmail("userupdate@mail.ru");
        userDto.setNickname("userupdate");
        userDto.setRoleName("ROLE_USER");

        mvc.perform(put("/api/admin/update/{id}", 1L)
                        .header("Authorization", jwtToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userDto)))
                .andExpect(status().isOk());

        User user = em.createQuery("SELECT u FROM User u JOIN FETCH u.role WHERE u.id = 1", User.class).getSingleResult();


        Assert.assertEquals("userupdate@mail.ru", user.getEmail());
        Assert.assertEquals("userupdate", user.getNickname());
        Assert.assertEquals("ROLE_USER", user.getRole().getName());
    }

}
