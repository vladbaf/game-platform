package kata.education.platform.gameplatform.tests.api;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.SeedStrategy;
import kata.education.platform.gameplatform.model.dto.request.RegisterDto;
import kata.education.platform.gameplatform.model.dto.request.AuthenticationRequestDto;
import kata.education.platform.gameplatform.model.dto.response.JwtResponse;
import kata.education.platform.gameplatform.model.entity.user.User;
import kata.education.platform.gameplatform.tests.config.AbstractSpringTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Интеграционные тесты контроллера  AuthenticationRestController
 */

public class AuthenticationRestControllerTests extends AbstractSpringTest {
    protected final String TEST_USER_LOGIN = "user@mail.ru";
    protected final String TEST_PASSWORD = "111";


    /**
     * Тест получения токена с данными тестового юзера и попытки получения токена с несуществующим в бд юзером
     */
    @Test
    @DataSet(value = {"dataset/authentication_rest_controller_dataset/users.yml", "dataset/authentication_rest_controller_dataset/role.yml"}, strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void authRestControllerGetTokenTest() throws Exception {
        String URL = "/api/auth/token";
        AuthenticationRequestDto authenticationRequestDto = new AuthenticationRequestDto(TEST_USER_LOGIN, TEST_PASSWORD);
        MvcResult mvcResult = mvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequestDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        JwtResponse jwtResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), JwtResponse.class);
        assertThat(jwtResponse.getLogin()).isEqualTo(TEST_USER_LOGIN);
        authenticationRequestDto = new AuthenticationRequestDto("notrealuser", "333");
        mvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authenticationRequestDto)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Тест на регистрацию нового юзера и попытка зарегистрировать юзера с уже имеющимся email в БД
     */
    @Test
    @DataSet(value = {"dataset/authentication_rest_controller_dataset/register_user/users.yml", "dataset/authentication_rest_controller_dataset/register_user/role.yml"}, strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void shouldRegistrationNewUser() throws Exception {
        RegisterDto registerDto = new RegisterDto("Biba2", "biba2@mail.com", "user");

        mvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(registerDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").value("Registration user success"));

        mvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(registerDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("User with email: biba2@mail.com exist in the database"));

        User user = (User) em.createQuery("FROM User WHERE email = 'biba2@mail.com'").getSingleResult();
        assertThat(user).isNotNull();
    }
}
