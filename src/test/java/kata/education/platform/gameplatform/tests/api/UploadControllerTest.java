package kata.education.platform.gameplatform.tests.api;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.SeedStrategy;
import kata.education.platform.gameplatform.model.entity.file.FileInfo;
import kata.education.platform.gameplatform.tests.config.AbstractSpringTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import javax.persistence.EntityManager;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UploadControllerTest extends AbstractSpringTest {

    protected String jwtToken;

    @Autowired
    EntityManager entityManager;

    @Test
    @DataSet(value = {"dataset/user_rest_controller_dataset/users.yml", "dataset/user_rest_controller_dataset/role.yml"}, strategy = SeedStrategy.CLEAN_INSERT, cleanAfter = true)
    public void uploadFileTest() throws Exception {
        jwtToken = getJwtToken("user100@mail.ru", "111");

        MockMultipartFile file = new MockMultipartFile("file", "Kata.txt", MediaType.TEXT_PLAIN_VALUE, "Hello, Kata!".getBytes());
        MockMultipartFile emptyFile = new MockMultipartFile("file", "", MediaType.TEXT_PLAIN_VALUE, "".getBytes());
        MockMultipartFile maxSizeFile = new MockMultipartFile("file", "Academy.txt", MediaType.TEXT_PLAIN_VALUE, new byte[1024 * 1024 * 10]);

        mvc.perform(multipart("/api/upload/single")
                .file(file)
                .header("Authorization", jwtToken))
                .andDo(print())
                .andExpect(status().isOk());

        FileInfo fileInfo = entityManager.createQuery("FROM FileInfo e Where name = :name", FileInfo.class)
                .setParameter("name", "Kata.txt")
                .getSingleResult();

        Assertions.assertEquals(1L, fileInfo.getId());
        Assertions.assertEquals("Kata.txt", fileInfo.getName());
        Assertions.assertEquals(file.getSize(), fileInfo.getSize());

        mvc.perform(multipart("/api/upload/single")
                        .file(emptyFile)
                        .header("Authorization", jwtToken))
                .andDo(print())
                .andExpect(jsonPath("$").value("The uploaded file was not found"));

        mvc.perform(multipart("/api/upload/single")
                        .file(maxSizeFile)
                        .header("Authorization", jwtToken))
                .andDo(print())
                .andExpect(jsonPath("$").value("The file size exceeds 1 MB"));

        FileInfo maxSizeFileInfo = entityManager.createQuery("FROM FileInfo e Where name = :name", FileInfo.class)
                .setParameter("name", "Kata.txt")
                .getSingleResult();

        Files.deleteIfExists(Path.of(System.getProperty("user.dir") + "\\src\\main\\resources\\uploads\\" + "user100@mail.ru" + "\\" + fileInfo.getKey() + ".txt"));
        Files.deleteIfExists(Path.of(System.getProperty("user.dir") + "\\src\\main\\resources\\uploads\\" + "user100@mail.ru" + "\\" + maxSizeFileInfo.getKey() + ".txt"));


    }


}
