package kata.education.platform.gameplatform;

import kata.education.platform.gameplatform.webapp.config.GamePlatformApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = GamePlatformApplication.class)
class GamePlatformApplicationTests {

    @Test
    void contextLoads() {
    }

}
