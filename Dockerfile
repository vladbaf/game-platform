FROM maven:3.8.6-openjdk-11 AS build
COPY src /game-platform/src
COPY pom.xml /game-platform
RUN mvn -f /game-platform/pom.xml clean package -Dmaven.test.skip

FROM amazoncorretto:18-alpine3.14
ARG JAR_FILE
COPY --from=build /game-platform/target/game-platform.jar /usr/local/lib/game-platform.jar
EXPOSE 8088
ENTRYPOINT ["java","-Dspring.profiles.active=dev","-jar","/usr/local/lib/game-platform.jar"]

